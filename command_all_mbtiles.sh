#!/bin/bash

deps=("Paris" "Seine-et-Marne" "Yvelines" "Essonne" "Hauts-de-Seine" "Seine-Saint-Denis" "Val-de-Marne")
ip=172.23.227.230
cluster_distance=255
printf -v sql_in_deps "'%s'," "${deps[@]}"
sql_in_deps=${sql_in_deps%,}

# docker run ghcr.io/osgeo/gdal:alpine-small-latest ogr2ogr -f GeoJSON /dev/stdout PG:"host=${ip} dbname=geonature2db29 user=geonatadmin password=dbpass port=5433" -sql "select * from gn_monitoring.vm_custom_snpn_mares" | ./tippecanoe -z18 -Bg -o tiles/pond.mbtiles --force;

docker run ghcr.io/osgeo/gdal:alpine-small-latest ogr2ogr -f GeoJSON /dev/stdout PG:"host=${ip} dbname=geonature2db29 user=geonatadmin password=dbpass port=5433" -sql "select area_name, geom from ref_geo.custom_departments where area_name in (${sql_in_deps})" | ./tippecanoe -zg -o tiles/all_deps.mbtiles  --force;

docker run ghcr.io/osgeo/gdal:alpine-small-latest ogr2ogr -f GeoJSON /dev/stdout PG:"host=${ip} dbname=geonature2db29 user=geonatadmin password=dbpass port=5433" -sql "select geom, nb_mare from gn_monitoring.vm_custom_snpn_nb_pond_in_inpn_cell" | ./tippecanoe -o tiles/pond_per_cell.mbtiles  --force;


#for dep in ${deps[@]};
#do docker run ghcr.io/osgeo/gdal:alpine-small-latest ogr2ogr -f GeoJSON /dev/stdout PG:"host=${ip} dbname=geonature2db29 user=geonatadmin password=dbpass port=5433" -sql "select 1 as count, * from gn_monitoring.v_custom_snpn_mares where area_name ilike '${dep}'" | ./tippecanoe -zg -o tiles/${dep}.mbtiles -r1 --cluster-distance=${cluster_distance} --accumulate-attribute=count:sum -B 5 --force;
#done
for dep in ${deps[@]};
  do docker run ghcr.io/osgeo/gdal:alpine-small-latest ogr2ogr -f GeoJSON /dev/stdout PG:"host=${ip} dbname=geonature2db29 user=geonatadmin password=dbpass port=5433" -sql "select * from gn_monitoring.vm_custom_snpn_mares where area_name ilike '${dep}'" | ./tippecanoe -z18 -B14  -r1 --cluster-distance=255 -o tiles/${dep}.mbtiles --force;
done

mbtiles_files=("${deps[@]/#/tiles/}")
./tile-join -o tiles/cluster.mbtiles "${mbtiles_files[@]/%/.mbtiles}" --force
#./tile-join -o tiles/cluster-pond.mbtiles tiles/cluster.mbtiles tiles/pond.mbtiles --force

rm -vf "${mbtiles_files[@]/%/.mbtiles}" 
