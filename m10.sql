CREATE MATERIALIZED VIEW gn_synthese.custom_martin_m10
TABLESPACE pg_default
AS SELECT count(s.id_synthese) AS count,
    la.geom
   FROM gn_synthese.synthese s
     JOIN gn_synthese.cor_area_synthese cas ON cas.id_synthese = s.id_synthese
     JOIN ref_geo.l_areas la ON la.id_area = cas.id_area
     JOIN ref_geo.bib_areas_types bat ON bat.id_type = la.id_type
  WHERE bat.type_code::text = 'M10'::text
  GROUP BY cas.id_area, la.geom
WITH DATA;